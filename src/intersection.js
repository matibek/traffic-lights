const TrafficLight = require('./trafficLight').TrafficLight;
const State = require('./trafficLight').State;

/**
 * A simple traffic intersection of two roads with four traffic lights.
 * 
 * @class Intersection
 */
class Intersection {
    /**
     * Creates an instance of a Traffic Intersection.
     * @param {string} name Identifier
     * @param {number} [switchInterval] Interval for switching traffic lights
     * @param {number} [warningDuration] Duration for showing warning light
     * @param {number} [totalDuration] Total duration of traffic lights
     */
    constructor(name, switchInterval, warningDuration, totalDuration) {
        this.name = name;
        this.switchInterval = switchInterval || 5 * 60 * 1000;
        this.warningDuration = warningDuration || 30 * 1000;
        this.totalDuration = totalDuration || 0;
        this._trafficLights = [
            new TrafficLight('(N, S)', State.GREEN),  // NOTE: it is also possible to ungroup them
            new TrafficLight('(E, W)', State.RED),
        ];
        this._warningState = false;
        this._currentDuration = 0;
    }

    /**
     * Indicates if the intersection is in warning state or not 
     * (i.e. at least one of the traffic light is in YELLOW color)
     * 
     * @returns {boolean}
     */
    isWarningState() {
        return this._warningState;
    }

    /**
     * Starts switching traffic lights for given duration
     * 
     * @param {number} [totalDuration] Total duration of traffic lights
     * @param {function} [callback] A callback function
     */
    switchLights(totalDuration, callback) {
        if (this._currentDuration !== 0) {
            throw new Error(`The intersection ${this.name} is in progress of switching lights!`)
        }
        this.totalDuration = totalDuration || this.totalDuration;
        this.callback = callback
        this._switch();
    }

    /**
     * [Private] Change the state of all the traffic lights periodically
     */
    _switch() {
        // Check current duration
        if (this._currentDuration > this.totalDuration) {
            let spentTime = this._currentDuration;
            this._startedTime = undefined; // reset to support new switch program
            this._currentDuration = 0;
            if (this.callback) {
                this.callback(spentTime);
            }
            return;
        }
        // Log state with current time
        console.log(`[${new Date().toISOString()}] ${this.toString()}`);

        // Schedule next switch
        let delay = this._warningState ? this.warningDuration : this.switchInterval - this.warningDuration;
        setTimeout(() => {
            this._currentDuration += delay;
            this._trafficLights.forEach((trafficLight) => {
                trafficLight.nextState(); // switch all
            });
            this._warningState = !this._warningState; // flip warning state
            this._switch();
        }, delay);
    }

    /**
     * A string representation of the current state of Intersection
     * 
     * @returns {string}
     */
    toString() {
        return `[${this.name}] ${this._trafficLights}`;
    }
}

module.exports = {
    Intersection: Intersection,
};
