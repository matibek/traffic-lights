/**
 * Traffic light states.
 * NOTE: Even though, we have 3 colors of traffic light, we use 4 states to represent them.
 * It is based on an assumption that a traffic light doesn't operates alone 
 * (i.e. a pair of traffic lights will have four possible combinations of states).
 */
const State = {
    GREEN: 1,
    YELLOW: 2,
    RED: 3,
    HOLDRED: 4, // This state represents a traffic light in RED color and its pair is YELLOW color.
};

/**
 * A traffic light with three colors
 * 
 * @class TrafficLight
 */
class TrafficLight {
    /**
     * Creates an instance of TrafficLight.
     * @param {string} name Identifier
     * @param {State} [state] Initial state
     */
    constructor(name, state) {
        this.name = name;
        this.state = state || State.RED;
    }

    /**
     * Change the state
     */
    nextState() {
        this.state = (this.state % 4) + 1
    }

    /**
     * Get the current color of the traffic light
     * 
     * @returns {string}
     */
    getColor() {
        if (this.state === State.GREEN) {
            return 'GREEN';
        }
        if (this.state === State.YELLOW) {
            return 'YELLOW';
        }
        return 'RED';
    }

    /**
     * A string representation of the current state of Traffic Light
     * 
     * @returns {string}
     */
    toString() {
        return ` ${this.name} is ${this.getColor()}`;
    }
}

module.exports = {
    State: State,
    TrafficLight: TrafficLight,
};
