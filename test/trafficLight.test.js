const expect = require('chai').expect;
const TrafficLight = require('../src/trafficLight').TrafficLight;
const State = require('../src/trafficLight').State;

describe('TrafficLight', () => {

    it('should initialize with default', (done) => {
        // PREPARE
        let trafficLight = new TrafficLight();
        // TEST
        expect(trafficLight).to.have.property('name');
        expect(trafficLight).to.have.property('state');
        expect(trafficLight.state).to.be.equal(State.RED);
        done();
    })

    it('should initialize with a custom values', (done) => {
        // PREPARE
        let name = 'light1'
        let trafficLight = new TrafficLight(name, State.GREEN);
        // TEST
        expect(trafficLight.name).to.be.equal(name);
        expect(trafficLight.state).to.be.equal(State.GREEN);
        done();
    })

    it('should change to next state', (done) => {
        // PREPARE
        let trafficLight = new TrafficLight();
        // TEST
        trafficLight.nextState();
        expect(trafficLight.state).to.be.equal(State.HOLDRED);
        trafficLight.nextState()
        expect(trafficLight.state).to.be.equal(State.GREEN);
        trafficLight.nextState()
        expect(trafficLight.state).to.be.equal(State.YELLOW);
        trafficLight.nextState()
        expect(trafficLight.state).to.be.equal(State.RED);
        done();
    })

    it('should get correct color', (done) => {
        // PREPARE
        let trafficLight = new TrafficLight();
        // TEST
        trafficLight.nextState();
        expect(trafficLight.getColor()).to.be.equal('RED');
        trafficLight.nextState()
        expect(trafficLight.getColor()).to.be.equal('GREEN');
        trafficLight.nextState()
        expect(trafficLight.getColor()).to.be.equal('YELLOW');
        trafficLight.nextState()
        expect(trafficLight.getColor()).to.be.equal('RED');
        done();
    })
})