const expect = require('chai').expect;
const Intersection = require('../src/intersection').Intersection;
const State = require('../src/trafficLight').State;

describe('Intersection', () => {
    
    describe('#Initialize', () => {

        it('should initialize with default', (done) => {
            // PREPARE
            let intersection = new Intersection();
            // TEST
            expect(intersection).to.have.property('name');
            expect(intersection).to.have.property('switchInterval');
            expect(intersection).to.have.property('warningDuration');
            expect(intersection).to.have.property('totalDuration', 0);
            expect(intersection).to.have.property('_trafficLights');
            expect(intersection._trafficLights).to.have.lengthOf(2); // expect two group
            let states = intersection._trafficLights.map((trafficLight) => {
                return trafficLight.state;
            });
            expect(states).to.have.members([State.GREEN, State.RED]);
            expect(intersection.isWarningState()).to.be.false;
            done();
        })

        it('should initialize with a custom values', (done) => {
            // PREPARE
            let name = 'intersection1'
            let intersection = new Intersection(name, 30, 3, 10);
            // TEST
            expect(intersection.name).to.be.equal(name);
            expect(intersection.switchInterval).to.be.equal(30);
            expect(intersection.warningDuration).to.be.equal(3);
            expect(intersection.totalDuration).to.be.equal(10);
            done();
        })

    })

    describe('#Switch Lights', () => {

        it('should switch lights for given duration', (done) => {
            // PREPARE
            let switchInterval = 30;
            let warningDuration = 3;
            let duration = 80;
            let intersection = new Intersection('Test1', switchInterval, warningDuration);
            // TEST
            intersection.switchLights(duration, (spentTime) => {
                let expectedTime = parseInt(duration / switchInterval) * switchInterval + (switchInterval - warningDuration);
                expect(spentTime).to.be.equal(expectedTime);
                expect(intersection.isWarningState()).to.be.true;
                done();
            });
        })

        it('should avoid switching lights when it is in progress', (done) => {
            // PREPARE
            let switchInterval = 20;
            let warningDuration = 3;
            let intersection = new Intersection('Test2', switchInterval, warningDuration);
            // TEST
            intersection.switchLights(20);
            expect(intersection.switchLights).to.throw();
            done();
        })

        it('should be able to switching lights more than one time', (done) => {
            // PREPARE
            let switchInterval = 20;
            let warningDuration = 3;
            let duration = 30;
            let intersection = new Intersection('Test2', switchInterval, warningDuration);
            // TEST
            intersection.switchLights(duration, (spentTime) => {
                expect(spentTime).to.be.not.null;
                intersection.switchLights(duration, (spentTime2) => {
                    expect(spentTime2).to.be.not.null;
                    done();
                });
            });
        })
        
        it('should change the states of all traffic lights', function(done) {
            // PREPARE
            let switchInterval = 30;
            let warningDuration = 10;
            let intersection = new Intersection('Test3', switchInterval, warningDuration);
            // TEST
            intersection.switchLights(1, (spentTime) => {
                expect(spentTime).to.be.equal(switchInterval - warningDuration);  // one switch
                expect(intersection.isWarningState()).to.be.true;
                let states = intersection._trafficLights.map((trafficLight) => {
                    return trafficLight.state;
                });
                expect(states).to.have.members([State.YELLOW, State.HOLDRED]);
                done();
            });
        })
        
    })
});