# Simulate Traffic Lights at Intersection
This is a simple [Node.js](https://nodejs.org/) app to simulates a set of traffic lights at an intersection. The traffic lights are designated (N, S) and (E, W) like a compass.

## How to run

Make sure you have [Node.js](https://nodejs.org/) installed.
```sh
$ git clone https://matibek@bitbucket.org/matibek/traffic-lights.git
$ cd traffic-lights
$ npm install
$ npm start
```

## Testing
To run the test programs, run:
```sh
$ npm test
```