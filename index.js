const Intersection = require('./src/intersection').Intersection;

const SWITCH_INTERVAL = 5 * 60 * 1000; // Interval for switching from green to red (in miliseconds)
const WARNING_DURATION = 30 * 1000; // Duration for showing warning (yellow) light
const TOTAL_DURATION = 30 * 60 * 1000;

let intersection = new Intersection('Main', SWITCH_INTERVAL, WARNING_DURATION);
console.log(`Switching lights of ${intersection.name} intersection...`);
intersection.switchLights(TOTAL_DURATION, (spentTime) => {
    console.log(`Finished switching lights of ${intersection.name} intersection.`);
});
